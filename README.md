# PointTest
Given a set (pointsValueSet) of points:

```
    [(1,1), (3,2), (1,-1), (2,4)]
```

Return the `kNumberRequested` number of points closest to origin (0,0).

For example, if `kNumberRequested=2` output will be:

```
2015-09-17 11:08:23.190 PointTest[75531:870175] -- number of points requested: 2
2015-09-17 11:08:23.191 PointTest[75531:870175] final point: 1.000000, 1.000000
2015-09-17 11:08:23.191 PointTest[75531:870175] final point: 1.000000, -1.000000
```

##License
PointTest is licensed under the Creative Commons (CC0) 1.0 Universal license.
