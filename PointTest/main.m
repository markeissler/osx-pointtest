//
//  main.m
//  PointTest
//
//  Created by Mark Eissler on 9/16/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

// Given a set (pointsValueSet) of points:
//    [(1,1), (3,2), (1,-1), (2,4)]
//
// Return the kNumberRequested number of points closes to origin (0,0).
//

#import <Foundation/Foundation.h>

const NSUInteger kNumberRequested = 2;

CGFloat distanceFromOrigin(CGPoint point);
NSArray *closestPointsInSetForMax(NSSet *set, NSUInteger max);

CGFloat distanceFromOrigin(CGPoint point)
{
  return hypotf(0.0 - point.x, 0.0 - point.y);
}

NSArray *closestPointsInSetForMax(NSSet *set, NSUInteger max)
{
  NSMutableArray *closestPoints = [NSMutableArray arrayWithCapacity:max];

  for (NSValue *value in set) {
    CGPoint newPoint = [value pointValue];
    
    // push initial values to list
    if (closestPoints.count < max) {
      closestPoints[closestPoints.count] = [NSValue valueWithPoint:newPoint];
      continue;
    }

    // is it in the list?
    for (NSUInteger i = 0; i < max; i++) {
      CGPoint oldPoint = [closestPoints[i] pointValue];
      if (distanceFromOrigin(newPoint) < distanceFromOrigin(oldPoint)) {
        closestPoints[i] = [NSValue valueWithPoint:newPoint];
        continue;
      }
    }
  }
  
  return closestPoints;
}

int main(int argc, const char * argv[]) {
  @autoreleasepool {

    NSSet *pointsValueSet = [[NSSet alloc] initWithArray: @[
      [NSValue valueWithPoint:CGPointMake(1,1)],
      [NSValue valueWithPoint:CGPointMake(3,2)],
      [NSValue valueWithPoint:CGPointMake(1,-1)],
      [NSValue valueWithPoint:CGPointMake(2,4)]
    ]];
    
    NSArray *closestPoints = closestPointsInSetForMax(pointsValueSet, kNumberRequested);
    NSLog(@"-- number of points requested: %ld", (unsigned long)kNumberRequested);
    for (NSValue *value in closestPoints) {
      CGPoint point;
      [value getValue:&point];
      NSLog(@"final point: %f, %f", point.x, point.y);
    }
  }
  return 0;
}
